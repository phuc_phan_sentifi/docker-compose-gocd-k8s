## Requirements

1. Install docker
2. Install minikube
3. Install kubectl

## Files/Folders structure

* `ssh-key/`: contains the ssh key for user `go` on docker container. You would need that ssh key for fetching source code from bitbucket or github
* `godata/`: go server data
* `kubeconfig.yaml`: shared with docker container `gocd-client-docker-kubectl`. Kubectl on that containers will use that cluster setup

## Getting started

1. Prepare your ssh key. Copy it to `ssh-key`
2. Run cmd: `docker-compose up`
3. Visit: http://localhost:8153

## Troubleshooting

1. My k8s jobs is not running and having issue with insufficiently memory/resources.
    * Start the minikube with more resource  
    On MacOS: minikube start --cpus 2 --memory 8192 --vm-driver hyperkit
